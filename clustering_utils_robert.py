import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth
from pylab import *
import pandas as pd

def column(matrix, i):
    return [row[i] for row in matrix]
    
# Compute clustering with MeanShift
def ms_clustering(x, b):

    # The following bandwidth can be automatically detected using
    # bandwidth = estimate_bandwidth(np.asarray(x), quantile=0.2)
    ms = MeanShift(bandwidth=b, bin_seeding=True)
    y_pred = ms.fit_predict(x)

    return(y_pred)

def prepare_data(new_x, b):
    x_column = np.expand_dims(column(new_x, 0),axis=1)
    clusters = ms_clustering(x_column,b)
    return(clusters)

def create_vectors(x, vertical_clusters):
    # create pandas dataframe and append cluster data of vertical clusters
    labels = ['tick','x','y','state']
    data = pd.DataFrame.from_records(x, columns=labels)
    data['cluster_ID'] = pd.Series(vertical_clusters, index=data.index)
    data = data.sort_values(by='tick')

    # drop duplicates according to segment value
    cl_data = data.groupby(["cluster_ID"])
    unique_data = []
    for key, item in cl_data:
        unique_data.append(cl_data.get_group(key).drop_duplicates("y"))
    unique_df = pd.concat(unique_data)

    # sort the data according to the segment value in each cluster
    y_dataframe = unique_df.groupby("cluster_ID")
    sorted_list = []
    for key, item in y_dataframe:
        sorted_list.append(y_dataframe.get_group(key).sort_values(by="y"))
    sorted_data = pd.concat(sorted_list)

    # extract the cluster order
    a = data.iloc[:,4].values
    cluster_order = [a[i] for i in sorted(np.unique(a, return_index=True)[1])]
    vectors = []
    for item in cluster_order:
        vectors.append(sorted_data[sorted_data["cluster_ID"] == item]["y"].values)
    return(vectors, data)

# Cut off stripes into 2 seperate ones (according to the two entries/exits)
def seperate_stripes(x):
    x_top = []
    x_bottom = []
    for item in x:
        if item[2] > 10:
            x_top.append(item)
        else:
            x_bottom.append(item)
    return(x_top, x_bottom)

def pairwise_cluster(vectors):
    labels = []
    i = 0
    for a in vectors:
        if i % 2 == 0:
            if i != len(vectors)-1: # don't compare last one
                a = vectors[i]
                b = vectors[i+1]
                value = compare_similarity(a,b)
                if value == True:
                    labels.append([i, i +1])


        i += 1

    return(labels)

def compare_similarity(a,b):
    if a.size != b.size:
        if a.size > b.size:
            c = np.in1d(a,b)
        elif b.size > a.size:
            c = np.in1d(b,a)
    else: 
        c = np.in1d(a,b)
    if np.sum(c)/c.size < 0.5: # depending on threshold
        booleans = []
        for a_value, c_value in zip(a, c):
            if c_value == False:
                for v in b:

                    # numpy allow small differences in y 
                    boolean = np.isclose(a_value, v, atol = 1.4) # or rtol 1?
                    booleans.append(boolean)
                        
        if booleans != []:
            if np.sum(booleans)/a.size >= 0.5:
                value = True
            else:
                value = False
        else: 
            print("Empty input!", booleans)
            
    elif np.sum(c)/c.size >= 0.5:
        value = True

    else: 
        value = False
    return(value)

def cluster_data(b, x):
    clusters = prepare_data(x, b)
    vectors, cl_data = create_vectors(x, clusters)
    labels = pairwise_cluster(vectors)
    input_data = get_clusters(cl_data, labels)
    return(input_data)

def get_clusters(cl_data, labels):
    print('Found '+ str(len(labels))+ ' clusters.')
    # clusters = []
    cl_data.loc[:, "clusterid"] = -1 # clusterid is the ID of the merged vertical cluster IDs
    cluster_index = 0
    for cluster in labels:
        uni_IDs = pd.Series(cl_data["cluster_ID"]).unique()
        i = (uni_IDs[cluster[0]])
        j = (uni_IDs[cluster[1]])
        i_indices = cl_data["cluster_ID"] == i
        j_indices = cl_data["cluster_ID"] == j
        cl_data.loc[i_indices,'clusterid'] = cluster_index
        cl_data.loc[j_indices,'clusterid'] = cluster_index
        cluster_index += 1
        # cl_part_1 = cl_data[cl_data["cluster_ID"] == i].loc[:,["tick", "x", "y", "state"]].values.tolist()
        # cl_part_2 = cl_data[cl_data["cluster_ID"] == j].loc[:,["tick", "x", "y", "state"]].values.tolist()

        # clus = cl_part_1 + cl_part_2
        # clusters.append(np.asarray(clus))

    return cl_data

