In this repository you find several algorithms to train and test neural networks for car detection:
e.g. to detect the direction of the car or to detect the type of vehicle passing the ParkHere sensor.

As input training set you need either a HDF data set which is already preprocessed by padding and min max norm (ask me) or a quilt data set, containing the raw classification data(ask Robert Seidl).