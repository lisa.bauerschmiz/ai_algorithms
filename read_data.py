import h5py
import numpy as np



### read data from hdf data set ###

def readHDF(path):

    with h5py.File(path,'r') as hdf:

                
        Cluster = hdf.get('Cluster')
   
        Cluster_Input = Cluster.get('Input')
        Cluster_Output = Cluster.get('Output')
        
        x_train = np.array(Cluster_Input.get('x_train_mc'))
        y_train = np.array(Cluster_Output.get('y_train_mc'))
        x_test = np.array(Cluster_Input.get('x_test_mc'))
        y_test = np.array(Cluster_Output.get('y_test_mc'))


        print('x train: ',np.shape(x_train),'y train: ',np.shape(y_train),'x test: ',np.shape(x_test),'y test: ',np.shape(y_test))


    return (x_train, y_train, x_test, y_test)


#convert cmp data to cn data
def convert_y(ys):
    
    new_y = []
    for y in ys:
        if y[3]== 1 or y[2] == 1:
            new_y.append(0)
        else:
            new_y.append(1)
    new_y = np.asarray(new_y)
    return(new_y)

def convert_2dd(xs,ys):
    
    new_x = []
    new_y = []
    for x,y in zip(xs,ys):
        if y[0]== 1:
            new_y.append(0)
            new_x.append(x)
        elif y[1] == 1:
            new_y.append(1)
            new_x.append(x)

    new_y = np.asarray(new_y)
    new_x = np.asarray(new_x)
    return(new_x, new_y)
