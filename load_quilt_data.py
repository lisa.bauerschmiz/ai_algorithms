import json
import quilt
import numpy as np

from quilt.data.parkhere.sensor import datasets

from collections import namedtuple

QUILT_PATH = 'parkhere/sensor/datasets/balancing'

#quilt.install("parkhere/sensor", force=True)

Data = namedtuple('Sample', 'data label count meta')

class QuiltStorage:
    @staticmethod
    def _fetch_node(selected_node, typ=None):
        node = None
        if selected_node in datasets.balancing._keys():
            node = datasets.balancing[selected_node]
            if typ:
                try:
                    node = node[typ]
                except:
                    raise KeyError('Node {} does not exist. Known nodes are: {}'.format(typ, node._keys()))
        else:
            raise KeyError('{} does not exist in {}. Known nodes are: {}'.format(selected_node, QUILT_PATH, datasets.balancing._keys()))


        return node

    
    @staticmethod
    def get(selected_node, typ=None, as_dict=False, ignore_other=True):
        """ reads the labeling data from the quilt repository 
            Returns: 
                Data of the leaf node or the node itself otherwise
                as_dict: bool, return all_data as large dictionary
                ignore_other: bool, skip all samples with label OTHER
        """
        node = QuiltStorage._fetch_node(selected_node, typ)

        if node and typ:
            dataset = np.load(open(node.data(),"rb"))
            labels = node.labels()
            counts = node.counts()
            with open(node.metadata()) as myjson:
                metadata = json.load(myjson)

            if as_dict:
                # expensive...
                all_data = {}
                for data, label, count, meta in zip(dataset, labels, counts, metadata):
                    if ignore_other == True:
                        if not "OTHER" in label:
                            all_data[meta['uuid']] = Data(data, label, count, meta)
                    else:
                        all_data[meta['uuid']] = Data(data, label, count, meta)
                
                return all_data

            return dataset, labels, counts, metadata
        
        return node

    @staticmethod
    def store(folder, quilt_node, typ, append=False, force=False):
        quilt_path = os.path.join(QUILT_PATH, quilt_node)

        if not os.path.exists(folder):
            raise KeyError('folder does not exist')
        else:
            # load data from folder 
            datasets = utility.read_datasets_from_directory(folder, type_=typ)

        node = QuiltStorage._fetch_node(quilt_node, typ)
        if node and not (force or append):
            warnings.warn('Node already exists. If you want to overwrite all data you need to apply the flag --force.\n If you want to append data, use the flag --apend'.format(quilt_node))
            return None
        
        
        all_data = {}

        # merge into one dictionary and apply corrections
        for dataset in datasets:
            for sample in dataset['dataset'].dataset:
                if sample.meta['uuid'] in corrections_dictionary.keys() and typ == 'sequences':
                    
                    new_label = corrections_dictionary[sample.meta['uuid']]['label']
                    new_count = corrections_dictionary[sample.meta['uuid']]['count']
                    tmp_sample = Data(sample.data, new_label, new_count, sample.meta)
                    sample = tmp_sample


                all_data[sample.meta['uuid']] = sample

        # convert dict entries to numpy arrays for easy storage
        dataset = np.array([data.data for data in all_data.values()])
        labels = np.array([data.label for data in all_data.values()])
        counts = np.array([data.count for data in all_data.values()])
        metadata = [data.meta for data in all_data.values()]

        # if appropriate, get the current data from the node and append the new data
        if append:
            node_data = QuiltStorage.get(quilt_node, typ)
            n_dataset, n_labels, n_counts, n_metadata = node_data

            # append the new data to the dataset
            dataset = np.hstack([n_dataset, dataset])
            labels = np.hstack([n_labels, labels])
            counts = np.hstack([n_counts, counts])
            n_metadata.extend(metadata)
            metadata = n_metadata

        # save dataset with numpy
        np.save('_dataset.npy', dataset)
        # save meta data for storage
        with open('_metadata.json', 'w') as outfile:
            json.dump(metadata, outfile)

        # create datanode 
        quilt.build(os.path.join(quilt_path, typ, 'data'), '_dataset.npy')
        quilt.build(os.path.join(quilt_path, typ, 'labels'), labels)
        quilt.build(os.path.join(quilt_path, typ, 'counts'), counts)
        quilt.build(os.path.join(quilt_path, typ, 'metadata'), '_metadata.json')

        quilt.push(quilt_path)
        print("Pushed dataset to quilt: {}".format(quilt_path))

        # cleanup of temporary file
        os.remove('_dataset.npy')
        os.remove('_metadata.json')

    @staticmethod
    def delete(selected_node, typ=None):
        """ deletes a node in quilt storage """
        if typ:
            try: 
                del datasets.balancing[selected_node][typ]
            except:
                raise KeyError('{}/{} does not exist.'.format(selected_node, typ))
        else:
            try:
                del datasets.balancing[selected_node]
            except:
                raise KeyError('{} does not exist.'.format(selected_node))
            
        # commit to local repo
        quilt.build(QUILT_PATH, datasets.balancing)

        # push to quilt
        quilt.push(QUILT_PATH)

    @staticmethod
    def credentials():
        return {
            'user': 'parkhere',
            'login': 'bfub1iBl80IC'
        }



corrections_dictionary = {'051546a9acde4d3b963a87af4dc0f42c': {
                                'count': -2,
                                'id': '051546a9acde4d3b963a87af4dc0f42c',
                                'label': 'CAR_OUT; CAR_OUT'
                                },
                           '76cc0599c50a439395b4498149cb4c14': {
                                'count': -2,
                                'id': '76cc0599c50a439395b4498149cb4c14',
                                'label': 'CAR_OUT; CAR_OUT'
                                },
                            '789f7c4280244276834e29514b7d0b15': {
                                'count': -4,
                                'id': '789f7c4280244276834e29514b7d0b15',
                                'label': 'CAR_OUT; CAR_OUT; CAR_OUT; CAR_OUT'
                                },
                            'a8ac8c4d16a04d3894ef34e1b9e8f23e': {
                                'count': -6,
                                'id': 'a8ac8c4d16a04d3894ef34e1b9e8f23e',
                                'label': 'CAR_OUT; CAR_OUT; CAR_OUT; CAR_OUT; CAR_OUT; CAR_OUT'
                                },
                            'b050984aff0a42be82e7eec7ac5d70b3': {
                                'count': -1,
                                'id': 'b050984aff0a42be82e7eec7ac5d70b3',
                                'label': 'CAR_OUT; PEDESTRIAN'
                                },
                            'f1b31bb1c59744f29184458ad49ff670': {
                                'count': -2,
                                'id': 'f1b31bb1c59744f29184458ad49ff670',
                                'label': 'CAR_OUT; CAR_OUT'
                            },
                            '77f524b687ff4754b28174bee12d3295': {
                                'count': -2,
                                'id': '77f524b687ff4754b28174bee12d3295',
                                'label': 'CAR_OUT; CAR_OUT'
                            },
                            'c8216561ba964e2fbebd2cfb4c365d53': {
                                'count': 0,
                                'id': 'c8216561ba964e2fbebd2cfb4c365d53',
                                'label': 'PEDESTRIAN'
                            }}

def load_data(selected_node, typ=None):
    return QuiltStorage.get(selected_node, typ)

def load_data_dict(selected_node, typ, ignore_other=True):
    return QuiltStorage.get(selected_node, typ, as_dict=True, ignore_other=ignore_other)