import numpy as np
import itertools
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from keras.models import load_model
from sklearn import metrics


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.show()


### plot graph during training of the network ###

def plot_data(training_loss,val_loss, epochs):

    plt.figure(figsize=(10, 4))
    x_ax = np.arange(1, len(training_loss)+1,1)
    plt.plot(x_ax, val_loss, 'bo', label='Test accuracy')
    plt.plot(x_ax, training_loss, label='Training accuracy')
    plt.legend()
    plt.xlabel('Nr. of epochs')
    plt.ylabel('Accuracy')
    plt.title('Training Performance after '+ str(len(training_loss))+' epochs, '+ 'accuracy: '+ str(training_loss[-1]))
    plt.show()

### to check the data of false predictions ###

def get_wrong_pred_data(y_pred_labels, y_true_labels, x_test):

    wrong_preds = y_pred_labels-y_true_labels
    wrong_x = []
    wrong_y_preds = []
    y_trues = []
    for wp, x, y_pred, y_true in zip(wrong_preds, x_test, y_pred_labels,y_true_labels):
        if wp != 0:
            wrong_x.append(x)
            wrong_y_preds.append(y_pred)
            y_trues.append(y_true)
    return(wrong_x, wrong_y_preds,y_trues)

def without_padding(x_data):
    new_x = []
    for i in x_data:
        if -1 not in i:
            new_x.append(i)
    new_x = np.asarray(new_x)
    return(new_x)

### to plot the data of false predictions ###

def plot_sensor_data(x_data, wrong_y_pred, y_true):
    new_x = without_padding(x_data)
    
    fig = plt.figure(figsize=(10,8))
    ax = fig.add_subplot(111) 
    
    #subplots
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212, sharex=ax1, sharey=ax1)
    
    #plot according to push/release and which sensor
    for x in new_x:
        if x[1] == 0:
            if x[3] == 0:
                ax1.plot(x[0], x[2], marker='o', fillstyle = 'none')
            else:
                ax1.plot(x[0], x[2], marker = 'o')
        else: 
            if x[3] == 0:
                ax2.plot(x[0], x[2], marker='o', fillstyle = 'none')
            else:
                ax2.plot(x[0], x[2], marker = 'o')
    
    # turn off spines and labels of overlayed plot (for the shared axes)
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
    ax1.tick_params(labelcolor='w', top='off', bottom='off', right='off', labelbottom = 'off')
    ax1.tick_params(labelcolor='black', left = 'on')
    ax1.set_ylim([0, 16])
    ax2.set_ylim([0, 16])
    # Set common labels
    ax.set_xlabel('time')
    ax.set_ylabel('segment[1/3m]')

    plt.suptitle('Prediction: '+str(wrong_y_pred)+' True value: '+str(y_true)+ ('\n0= Car in, 1= Car out, 2= Motorcycle, 3= Pedestrian'), va = 'bottom')
    plt.show()
    

### to plot confusion matrix and optionally the data of false predictions ###

def plot_wrongs(sample, x_test, y_test, model_path):
    
    model = load_model(model_path)
    layer_output = model.predict(x_test, verbose = 0)
    if 'multi' in model_path:
        y_true_labels = np.argmax(y_test, axis=1)  # only necessary if output has one-hot-encoding, shape=(n_samples)
        y_pred_labels = np.argmax(layer_output, axis=1)  # only necessary if output has one-hot-encoding, shape=(n_samples)
        confusion_matrix = metrics.confusion_matrix(y_true=y_true_labels, y_pred=y_pred_labels)  # shape=(12, 12)
        wrong_x, wrong_y_preds,y_trues = get_wrong_pred_data(y_pred_labels, y_true_labels, x_test)
        class_names = ('Car_in', 'Car_out', 'Motorcycle', 'Pedestrian')

    else:        
        print(layer_output[0],y_test[0])
        layer_output = np.round(layer_output)
        confusion_matrix = metrics.confusion_matrix(y_true=y_test, y_pred=layer_output) 
        y_test = np.expand_dims(y_test, axis=1)

        wrong_x, wrong_y_preds,y_trues = get_wrong_pred_data(layer_output, y_test, x_test)
        class_names = ('Car_in', 'Car_out')

    plt.figure()
    plot_confusion_matrix(confusion_matrix, classes=class_names,
                          title='Confusion matrix')
    
    print('Amount of wrong samples: '+ str(len(wrong_x)))
    x_data = wrong_x[sample]
    y_pred = wrong_y_preds[sample]
    y_true = y_trues[sample]

    plot_sensor_data(x_data, y_pred, y_true)


    
