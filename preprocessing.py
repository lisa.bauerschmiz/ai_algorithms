import numpy as np
from sklearn import preprocessing


def normalise_data(X):
    final_ticks = []
    final_modules = []
    for xi2 in X:
        ticks = []
        modules = []
        for item in xi2:
            ticks.append(item[0])
            modules.append(item[2])
        min_seg = min(modules)
        normalised_segs = []
        for item in xi2:
            normalised_segs.append(item[2]- min_seg)
        ticks = np.reshape(ticks,(-1, 1))
        modules = np.reshape(modules,(-1, 1))

        min_max_scaler = preprocessing.MinMaxScaler()
        ticks_minmax = min_max_scaler.fit_transform(ticks)
        final_ticks.append(ticks_minmax)
        final_modules.append(normalised_segs)
        
    
    X_first = []
    for xi3, minmax_ticks, minmax_modules in zip(X, final_ticks, final_modules):
        stripes = []
        release_pushs = []
        for i in xi3:

                stripes.append(i[1])
                release_pushs.append(i[3])
        stripes = np.asarray(stripes)
        release_pushs = np.asarray(release_pushs)
        X_first_item = np.column_stack((minmax_ticks, stripes, minmax_modules, release_pushs ))
            
        X_first.append(X_first_item)
        
    
    return(X_first)

def fit_input_data(X):
    
    X_new = []
    for xi in X:


        if len(xi) != 61:
            xi = xi.tolist()
            for i in range(61-len(xi)):
                xi.append([-1,-1,-1,-1])

            x_new = np.asarray(xi)
            X_new.append(x_new)

    X_new = np.asarray(X_new)

    return(X_new)