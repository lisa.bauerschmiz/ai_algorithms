import keras
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Convolution1D, MaxPooling1D, Flatten, BatchNormalization, Dense
from keras import metrics
from keras.activations import relu, sigmoid, softmax
from keras import backend as K
from keras import optimizers
from keras.optimizers import Adam, SGD 
from visualise_data import plot_data

### network configurations ###

class Net(object):


    def __init__(self, batch_size, epochs, optimizer, activation, minDelta, patience ):


        self.batch_size = batch_size
        self.epochs = epochs
        self.optimizer = optimizer
        self.activation = activation
        self.minDelta = minDelta
        self.patience = patience

    ##### Creation of the model
    def createModel(self):
        model = Sequential()
        # for the input shape keras ignores first dimension (batch size)
        model.add(Convolution1D(filters=10, kernel_size=7, activation=self.activation, padding='same', input_shape=(61, 4))) 
        model.add(MaxPooling1D())
        model.add(Convolution1D(filters=20, kernel_size=7, padding='same', activation=self.activation)) 
        model.add(MaxPooling1D())
        model.add(Convolution1D(filters=40, kernel_size=7 ,padding='same', activation=self.activation)) 
        model.add(MaxPooling1D())
        model.add(Flatten())
        model.add(Dense(30))
        model.add(Dense(4,activation=softmax))

        model.summary()
        return(model)

    ##### Execution/Training of the model
    def exeModel(self, x_train, x_test, y_train, y_test, modelPath):
        model = self.createModel()
        esCallBack = keras.callbacks.EarlyStopping(min_delta=self.minDelta, patience=self.patience)

        model.compile(loss='binary_crossentropy',
                      optimizer=self.optimizer,metrics=["accuracy"])
        history = model.fit(x_train, y_train,
                            batch_size=self.batch_size,
                            epochs=self.epochs,
                            verbose=1,
                            validation_data=(x_test, y_test), callbacks = [esCallBack])
    

        model.save(modelPath)
        plot_data(history.history['acc'],history.history['val_acc'], self.epochs)
        return('Done')    
    
    def exeModel_100_percent(self, x_train,y_train):
        model = self.createModel()
        esCallBack = keras.callbacks.EarlyStopping(min_delta=self.minDelta, patience=self.patience)

        model.compile(loss='binary_crossentropy',
                      optimizer=self.optimizer,metrics=["accuracy"])
        history = model.fit(x_train, y_train,
                            batch_size=self.batch_size,
                            epochs=self.epochs,
                            verbose=1,
                            validation_data=(x_test, y_test), 
                            callbacks = [esCallBack])
    

        model.save('D:\\ParkHere\\Keras_models\\model_'+'multi_class'+'.h5')
        plot_data(history.history['acc'],history.history['val_acc'], self.epochs)

        return('Done')
        #return(history.history['loss'],history.history['val_loss'])

class dd_Net(object):


    def __init__(self, batch_size, epochs, optimizer, activation, minDelta, patience ):


        self.batch_size = batch_size
        self.epochs = epochs
        self.optimizer = optimizer
        self.activation = activation
        self.minDelta = minDelta
        self.patience = patience

    ##### Creation of the model
    def createModel(self):
        model = Sequential()
        # for the input shape keras ignores first dimension (batch size)
        model.add(Convolution1D(filters=10, kernel_size=10, activation=self.activation, input_shape=(61, 4))) 
        model.add(MaxPooling1D())       
        model.add(BatchNormalization())
        model.add(Flatten())
        model.add(Dense(30))
        model.add(Dense(1,activation=self.activation))

        model.summary()
        return(model)

    ##### Execution/Training of the model
    def exeModel(self, x_train, x_test, y_train, y_test, modelPath):
        model = self.createModel()
        esCallBack = keras.callbacks.EarlyStopping(min_delta=self.minDelta, patience=self.patience, restore_best_weights = True)

        model.compile(loss='binary_crossentropy',
                      optimizer=self.optimizer,metrics=["accuracy"])
        history = model.fit(x_train, y_train,
                            batch_size=self.batch_size,
                            epochs=self.epochs,
                            verbose=1,
                            validation_data=(x_test, y_test), callbacks = [esCallBack])

        model.save(modelPath)
        plot_data(history.history['acc'],history.history['val_acc'], self.epochs)
        return('Done')
        #return(history.history['loss'],history.history['val_loss'])

